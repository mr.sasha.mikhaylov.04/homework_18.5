#include <iostream>
#include <string>

using namespace std;

class Player {

public:
    void ShowPlayerInfo() {
        cout << playerName << " " << playerScore << "\n";
    }

    Player() : playerName("Default"), playerScore(0) {

    }

    Player(string name, int score) : playerName(name), playerScore(score) {
        
    }

    int GetPlayerScore() {
        return playerScore;
    }

private:
    string playerName;
    int playerScore;
};

void ShowPlayersStats(Player *a, int playersNum) {

    cout << "-------------------\n";

    for (int i = 0; i < playersNum; i++)
    {
        a[i].ShowPlayerInfo();
    }

}

void BubbleSorting(Player* array, int size) {
    bool isSortOver = false;

    while (!isSortOver) {
        isSortOver = true;

        for (Player* i = &array[0]; i < array + size - 1; i++)
        {
            if (i->GetPlayerScore() < (i + 1)->GetPlayerScore()) {
                swap(*i, *(i + 1));
                isSortOver = false;
            }
        }
    }
}

int main()
{
    int playerScore;
    int playersNumber;

    string playerName;

    cout << "Number of players: "; 
    cin >> playersNumber;

    Player *players;
    players = new Player[playersNumber];

    for (int i = 0; i < playersNumber; i++)
    {
        cout << "Player " << i + 1 << " name: ";
        cin >> playerName;

        cout << "Player's score: ";
        cin >> playerScore;

        Player newPlayer(playerName, playerScore);

        players[i] = newPlayer;
    }
    
    ShowPlayersStats(players, playersNumber);

    BubbleSorting(players, playersNumber);
    
    ShowPlayersStats(players, playersNumber);

    delete[] players;
}

